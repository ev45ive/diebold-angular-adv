import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { YesnoPipe } from './yesno/yesno.pipe';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RecentSearchesComponent } from './recent-searches/recent-searches.component';
import { ToasterNotificationsComponent } from './toaster-notifications/toaster-notifications.component';



@NgModule({
  declarations: [YesnoPipe, RecentSearchesComponent, ToasterNotificationsComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [
    YesnoPipe,
    FormsModule,
    ReactiveFormsModule,
    RecentSearchesComponent,
    ToasterNotificationsComponent,
  ]
})
export class SharedModule { }
