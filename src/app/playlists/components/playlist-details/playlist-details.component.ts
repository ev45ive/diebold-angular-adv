import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Playlist } from 'src/app/core/model/playlist';

@Component({
  selector: 'app-playlist-details',
  templateUrl: './playlist-details.component.html',
  styleUrls: ['./playlist-details.component.scss']
})
export class PlaylistDetailsComponent implements OnInit {

  @Input() playlist!: Playlist

  @Output() edit = new EventEmitter<void>();

  constructor() {
  }

  editClick(){
    this.edit.emit()
  }

  ngOnInit(): void {
    if (!this.playlist) { throw new Error('[playlist] is required') }
  }

}
