import { Inject, Injectable } from '@angular/core';
import { Playlist } from '../../model/playlist';
import { PlaylistsSource, PLAYLISTS_INIT_DATA } from '../../tokens';


@Injectable({
  // providedIn: CoreModule
  providedIn: 'root'
})
export class PlaylistsService implements PlaylistsSource{

  constructor(
    @Inject(PLAYLISTS_INIT_DATA) private playlists: Playlist[] = []) {

    console.log(playlists)
  }

  getPlaylists() {
    return this.playlists
  }

  savePlaylist(draft: Playlist) {
    this.playlists = this.playlists.map(
      p => p.id === draft.id ? draft : p)
  }

}
