import { Playlist } from '../model/playlist';

export const playlistsMock: Playlist[] = [
  {
    id: '123',
    name: 'Awesome playlist 123',
    public: true,
    description: 'I love this playlist'
  },
  {
    id: '234',
    name: 'Awesome playlist 234',
    public: false,
    description: 'I hate this playlist'
  },
  {
    id: '345',
    name: 'Awesome playlist 345',
    public: true,
    description: 'I know this playlist'
  },
]
