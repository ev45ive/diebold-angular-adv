import { HttpClient, HttpClientModule } from '@angular/common/http';
import { NgModule, Optional, SkipSelf } from '@angular/core';
import { environment } from 'src/environments/environment';
import { AuthModule } from './auth/auth.module';
import { Playlist } from './model/playlist';
import { PLAYLISTS_INIT_DATA, PlaylistsSource, API_URL } from './tokens';

@NgModule({
  imports: [
    HttpClientModule,
    AuthModule
  ],
  providers: [
    // {provide:HttpClient,useClass:MyMuchAwesomerHttpClient,}
    {
      provide: API_URL,
      useValue: environment.api_url
    },
    {
      provide: PLAYLISTS_INIT_DATA,
      useValue: []
    },
    // Factory:
    // {
    //   provide: PlaylistsService,
    //   useFactory(data: Playlist[]/* valueB, instanceC */) {
    //     return new PlaylistsService(data)
    //   },
    //   deps: [PLAYLISTS_INIT_DATA/* tokenB, tokenC */]
    // },
    // Autowire:
    // {
    //   provide: AbstractPlaylistsService,
    //   useClass: SpotifyPlaylistsService,
    //   // deps: [PLAYLISTS_INIT_DATA]
    // },
    // {
    //   provide: PlaylistsService,
    //   useClass: PlaylistsService,
    // },
    // Inject by "pseudo-" interface (token)
    // {
    //   provide: PlaylistsSource,
    //   useClass: PlaylistsService
    // }
    // PlaylistsService,

    // Alias:
    // {provide: MusicProvider, useExisting: PlaylistsService},
    // {provide: MovieProvider, useExisting: PlaylistsService},
    // {provide: ShowsProvider, useExisting: PlaylistsService},
  ]
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() core: CoreModule) {
    if (core !== null) {
      throw Error('Core should be included only ONCE in root module')
    }
  }
}
