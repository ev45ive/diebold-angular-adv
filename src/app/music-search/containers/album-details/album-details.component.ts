import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map, pluck, share, shareReplay, switchMap } from 'rxjs/operators';
import { Track } from 'src/app/core/model/search';
import { MusicSearchService } from 'src/app/core/services/music-search/music-search.service';

@Component({
  selector: 'app-album-details',
  templateUrl: './album-details.component.html',
  styleUrls: ['./album-details.component.scss']
})
export class AlbumDetailsComponent implements OnInit {

  album_id = this.route.params.pipe(
    pluck('album_id')
  ) as Observable<string>

  album = this.album_id.pipe(
    switchMap(id => this.service.getAlbumById(id)),
    shareReplay()
  )

  currentTrack?: Track
  tracks = this.album.pipe(map(album => album.tracks.items))

  @ViewChild('audioRef')
  audioRef?: ElementRef<HTMLAudioElement>

  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.

    if (this.audioRef?.nativeElement) {
      this.audioRef.nativeElement.volume = 0.2
    }
  }


  constructor(
    private route: ActivatedRoute,
    private service: MusicSearchService
  ) { }

  trackSelect(track: Track) {
    this.currentTrack = track;

    setTimeout(() => {
      if (this.audioRef?.nativeElement) {
        this.audioRef.nativeElement.play()
      }
    })
  }

  ngOnInit(): void {

  }

}
