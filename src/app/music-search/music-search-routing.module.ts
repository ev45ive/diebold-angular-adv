import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AlbumDetailsComponent } from './containers/album-details/album-details.component';

import { MusicSearchComponent } from './music-search.component';

const routes: Routes = [
  { path: /* music/ */'', redirectTo: 'search', pathMatch: 'full' },
  { path: 'search', component: MusicSearchComponent },

  // http://localhost:4200/music/albums/5Tby0U5VndHW0SomYO7Id7/details
  {
    path: 'albums/:album_id/details'
    , component: AlbumDetailsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MusicSearchRoutingModule { }
