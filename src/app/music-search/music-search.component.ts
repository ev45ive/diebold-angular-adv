import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ConnectableObservable, Observable, ReplaySubject, Subject, Subscription } from 'rxjs';
import { filter, map, multicast, publishReplay, refCount, shareReplay, takeUntil } from 'rxjs/operators';
import { Album } from '../core/model/search';
import { MusicSearchService } from '../core/services/music-search/music-search.service';

@Component({
  selector: 'app-music-search',
  templateUrl: './music-search.component.html',
  styleUrls: ['./music-search.component.scss']
})
export class MusicSearchComponent implements OnInit {
  showRecent = false

  query = this.service.queryChanges
  message = this.service.recentErorrs.pipe(map((err: any) => err?.message))
  results = this.service.resultsChanges
  // results!: Observable<Album[]>
  // this.results = this.service.makeSearchRequest(query).pipe(
  //   shareReplay() // multicast( new ReplaySubject() ) + refCount()
  // )


  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: MusicSearchService) { }

  searchAlbums(query: string) {
    this.router.navigate([/* '/music/search' */], {
      relativeTo: this.route,
      queryParams: { q: query },
    })
  }

  ngOnInit(): void {
    // const q = this.route.snapshot.queryParamMap.get('q')

    this.route.queryParamMap.pipe(
      map(paramMap => paramMap.get('q')),
      filter(q => q != null)
    ).subscribe(query => this.service.searchAlbums(query!))
  }


}
