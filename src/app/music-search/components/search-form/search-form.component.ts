import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges, ɵɵtrustConstantResourceUrl } from '@angular/core';
import { AbstractControl, AsyncValidatorFn, FormArray, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { combineLatest, Observable, Observer, Subscriber } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, map, withLatestFrom } from 'rxjs/operators';

// // index.d.ts 
// declare global {
//   interface Window{
//     form: FormGroup
//   }
// }
const combined = Validators.compose([
  Validators.required, Validators.minLength(3)
])!

const censor: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
  const badword = 'batman';
  const hasError = String(control.value).includes(badword)
  return hasError ? {
    'censor': { badword }
  } : null
  // return {required:true, minlength: { requiredLength: 123 }}
}

const asyncCensor: AsyncValidatorFn = (control: AbstractControl): Observable<ValidationErrors | null> => {
  // return this.http.get('/valudae?q='+control.value).pipe(map(res => errors | null ))

  return new Observable((subscriber: Observer<ValidationErrors | null> /* Subscriber */) => {
    // console.log('on subscribe')

    const result = censor(control)
    const handle = setTimeout(() => {
      // console.log('on next')
      subscriber.next(result)
      subscriber.complete()
    }, 200)

    return () => {
      // console.log('on unsubscribe');
      clearInterval(handle)
    }
  })
}

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss']
})
export class SearchFormComponent implements OnInit {
  @Output() search = new EventEmitter<string>();

  @Input() set query(query: string | null){
    (this.searchForm.get('query') as FormControl).setValue(query,{
      emitEvent:false,
      // onlySelf:true
    })
  }

  ngOnChanges(changes: SimpleChanges): void {
    //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    //Add '${implements OnChanges}' to the class.
    // this.searchForm.get('query')!.setValue(changes['query'].currentValue)
  }

  searchForm = new FormGroup({
    query: new FormControl('', [
      // combined,
      Validators.required,
      // Validators.requiredTrue, // checkbox
      Validators.minLength(3),
      // censor
    ], [
      asyncCensor
    ]),
    extras: new FormGroup({
      type: new FormControl('album'),
      markets: new FormArray([
        new FormGroup({
          code: new FormControl('PL')
        })
      ])
    })
  }, [])

  markets = this.searchForm.get(['extras', 'markets']) as FormArray

  extras = false

  constructor() {
    (window as any).form = this.searchForm
  }

  ngOnInit(): void {
    const queryField = this.searchForm.get('query') as FormControl

    const valueChanges = queryField.valueChanges as Observable<string>
    const statusChanges = queryField.statusChanges as Observable<'VALID' | 'INVALID' | 'PENDING' | 'DISABLED'>


    const validValues = statusChanges.pipe(
      debounceTime(400),
      withLatestFrom(valueChanges),
      filter(([status, value]) => status === 'VALID'),
      map(([status, value]) => value),
    )

    const searches = validValues.pipe(
      filter(q => q.length >= 3),
      distinctUntilChanged(),
    )

    // How chain of pipe operators works:
    // node_modules\rxjs\internal\operators\map.js

    searches.subscribe(this.search)
  }

  addMarket() {
    this.markets.push(
      new FormGroup({
        code: new FormControl('')
      }))
  }

  removeMarket(i: number) {
    this.markets.removeAt(i)
  }


  submitSearch() {
    // debugger
    this.search.emit(this.searchForm.get('query')!.value)
  }


}
